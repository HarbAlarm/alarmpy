from collections import namedtuple
from multiprocessing import Process, Queue
from datetime import datetime
import datetime as fulldatetime
import os, sys, time, vlc

import gpio

running = True
alarming = False

alarm = namedtuple
if len(sys.argv):
	alarm.h = sys.argv[1]
	alarm.m = sys.argv[2]
else:
	alarm.h = 7
	alarm.m = 30
alarm.next = 0
alarm.allowedattempts = 5
alarm.totalattempts = 0
queue = ""

# check if Mplayer installed
_found_mplayer = None
for dr in os.environ["PATH"].split(os.pathsep):
    if os.path.exists(os.path.join(dr, "mplayer")):
        _found_mplayer = dr
if not _found_mplayer:
    print("Error: Mplayer required !")
    sys.exit()


alarm.next = datetime.timestamp(datetime(time.localtime().tm_year, time.localtime().tm_mon, time.localtime().tm_mday, int(alarm.h), int(alarm.m)))
v = vlc.MediaPlayer('02. The First Layer.flac')

q = Queue()
p = Process(target=gpio.f, args=(q,))
p.start()

while running:
	ts = time.time()
	if not q.empty():
		queue = q.get_nowait()

	if ts > alarm.next and not alarming:
		v.play()
		alarming=True
	
	if alarming:
		if queue == "snooze":
			if alarm.totalattempts < alarm.allowedattempts:
				v.stop()
				alarm.next += 300
				alarm.totalattempts +=1
				alarming=False
				v = vlc.MediaPlayer('02. The First Layer.flac')
			else:
				print("too bad")
		
		if queue == "stop":
			v.stop()
			alarm.totalattempts = 0
			alarming=False
			now = time.localtime()
			alarm.next = datetime.timestamp(datetime(now.tm_year, now.tm_mon, now.tm_mday, int(alarm.h), int(alarm.m)) + fulldatetime.timedelta(days=1))
	

