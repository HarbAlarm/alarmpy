# Music wish/todolist

## Easy:

- MiA Made in Abyss
- MiA The First Layer
- MiA Theme of Reg
- MiA Nanachi in the Dark
- MiA Tomorrow
 
- Cel Resurrections
- Cel Beginnings
- Cel Awake
- Cel Golden
- Cel Exhale
- Cel Little Goth

- Und It's raining somewhere else
- Und Ruins
- Und Uwa!! So Temperate
- Und Home
- Und Waterfall


## Normal:

- Cel First Steps
- Cel In the Mirror
- Cel Reach for the Summit

- Und Heartache


## Hard:

- MiA Sabertooth
- MiA Crucifixion

- Cel Scattered and Lost
- Cel Anxiety
- Cel Confronting Myself

- WakeupWakeupWakeup




## Other:


- Gravity Falls
- Kamex
- Shantae