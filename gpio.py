import time
import RPi.GPIO as gpio


def f(conn):
    gpio.setmode(gpio.BOARD)


    gpio.setup(23, gpio.IN, pull_up_down = gpio.PUD_DOWN)
    gpio.setup(24, gpio.IN, pull_up_down = gpio.PUD_DOWN)
    while True:
        time.sleep(.02)
        if(gpio.input(23) == 1):
            print("Button 1 pressed")
            conn.put("stop")
        if(gpio.input(24) == 1):
            print("Button 2 pressed")
            conn.put("snooze")




